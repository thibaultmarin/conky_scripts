#!/usr/bin/python
#
# IMAP access to gmail inbox and folders
#
# http://yuji.wordpress.com/2011/06/22/python-imaplib-imap-example-with-gmail/


# Imports
import imaplib
import email
import types
import re

# # # Parameters # # #

# Gmail labels to list
folders = ["inbox", "S/Action", "S/Next Action", "S/Action-ExtraProjects"]
# File with authentification information
filename = '.gauth'
# Maximum output width
max_width=50


#Enter your username and password below within double quotes
# eg. username="username" and password="password"
COMMENT_CHAR = '#'
OPTION_CHAR =  '='
 
def parse_config(filename):
    options = {}
    f = open(filename)
    for line in f:
        # First, remove comments:
        if COMMENT_CHAR in line:
            # split on comment char, keep only the part before
            line, comment = line.split(COMMENT_CHAR, 1)
        # Second, find lines with an option=value:
        if OPTION_CHAR in line:
            # split on option char:
            option, value = line.split(OPTION_CHAR, 1)
            # strip spaces:
            option = option.strip()
            value = value.strip()
            # store in dictionary:
            options[option] = value
    f.close()
    return options

# note that if you want to get text content (body) and the email contains
# multiple payloads (plaintext/ html), you must parse each message separately.
# use something like the following: (taken from a stackoverflow post)
def get_first_text_block(email_message_instance):
    maintype = email_message_instance.get_content_maintype()
    if maintype == 'multipart':
        for part in email_message_instance.get_payload():
            if part.get_content_maintype() == 'text':
                return part.get_payload()
    elif maintype == 'text':
        return email_message_instance.get_payload()

options = parse_config(filename)
username = options["user"]
password = options["pass"]

mail = imaplib.IMAP4_SSL('imap.gmail.com')
mail.login(username, password)
mail.list()
# Out: list of "folders" aka labels in gmail.

out_str = ''

for fold in folders:

	mail.select(fold) # connect to inbox.
	# Get email list in inbox
	result, data = mail.uid('search', None, "ALL")
	emails_uid = data[0].split()
	# Note: it should be possible to download all messages at once
	#result, email_data = mail.uid('fetch', ",".join(emails_uid), '(RFC822)')

	for euid in emails_uid:
		result, email_data = mail.uid('fetch', euid, '(RFC822)')
		raw_email = email_data[0][1]
		email_message = email.message_from_string(raw_email)
		content = get_first_text_block(email_message)
		if type(content) is types.NoneType:
			content = ""
		content = re.sub('[^a-zA-Z0-9 ]', ' ', content)
		content = re.sub('\s+', ' ', content)
		content = re.sub('^\s*', '', content)
		content = content[:max_width-2]
		author = email.utils.parseaddr(email_message['From'])[0]
		subject = email_message['Subject']
		auth_sub = author + "-" + subject
		out_str += auth_sub[:max_width] + '\n'
		if len(content) > 0:
			out_str += '  ' + content + '\n'

print out_str

