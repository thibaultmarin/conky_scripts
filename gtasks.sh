#!/bin/bash

# Get authentification
user=`cat .gauth | grep "user\s*=" | \
	sed -r 's/user\s*=\s*(.*)$/\1/g'`;

pass=`cat .gauth | grep "pass\s*=" | \
	sed -r 's/pass\s*=\s*(.*)$/\1/g'`;

# Get google authentification
auth=`curl --silent https://www.google.com/accounts/ClientLogin \
	--data-urlencode Email=$user --data-urlencode Passwd=$pass \
	-accountType=GOOGLE -d source=privacylog -d service=goanna_mobile | \
	grep -i "Auth="|sed -r 's/^\s*Auth\s*=\s*(.*)$/\1/g'`

# Get all tasks
tasks=`curl --silent --header "Authorization: GoogleLogin auth=$auth" \
	"https://mail.google.com/tasks/ig"`;

#tasks=`echo "$tasks" | grep "<script>function _init"`;
#tasks=`echo "$tasks" | sed -r 's#.*\],(\"name\":.*),\"user\".*#\1#g'`;

# Parse tasks (basic parsing)
tasks=`echo "$tasks" | tr ',' '\n' | grep "name"`;
tasks=`echo "$tasks" | sed -r 's/\"name\":\"(.*)\"/\1/g'`;

echo "$tasks";
