#!/bin/bash

# Get unread GMail emails from inbox.
#

# Get authentification
user=`cat .gauth | grep "user\s*=" | sed -r 's/user\s*=\s*(.*)$/\1/g'`;
pass=`cat .gauth | grep "pass\s*=" | sed -r 's/pass\s*=\s*(.*)$/\1/g'`;

# Get feed
rss=`curl -su $user:$pass https://mail.google.com/mail/feed/atom`;

# Get message count
count=`echo "$rss" | grep "<fullcount>" | sed -r \
	's#<fullcount>(.*)</fullcount>#\1#g'`;

# Parse message
if [ "$count" -gt 0 ];then
	echo "$count new messages";
	entry=`echo "$rss" | sed -n	'/<entry>/,/<\/entry>/p' \
	                   | grep -v -E "<.?entry>" \
	                   | grep -v "<link" \
	                   | grep -v "<modified" \
	                   | grep -v "<id>"`;
	authors=`echo "$entry" | grep "<name>" \
	                       | sed -r 's#<name>(.*)</name>#\1#i'`;
	emails=`echo "$entry" | grep "<email>" \
	                      | sed -r 's#<email>(.*)</email>#\1#i'`;
	titles=`echo "$entry" | grep "<title>" \
	                      | sed -r 's#<title>(.*)</title>#\1#i'`;
	conts=`echo "$entry" | grep "<summary>" \
	                     | sed -r 's#<summary>(.*)</summary>#\1#i'`;
	times=`echo "$entry" | grep "<issued>" \
	                     | sed -r 's#<issued>.*T(.*):[0-9]+Z</issued>#\1#i'`;
	for i in `seq 1 $count`;do
		author=`echo "$authors" | sed -n "${i}p"`;
		email=`echo "$emails" | sed -n "${i}p"`;
		title=`echo "$titles" | sed -n "${i}p" | head -c 30`;
		cont=`echo "$conts" | sed -n "${i}p" | head -c 60`;
		time=`echo "$times" | sed -n "${i}p"`;
		if [ "$author" == "" ];then
			authstr=$email;
		else
			authstr=$author;
		fi
		echo -e "$authstr <$time> - $title\n $cont";
	done
else
	echo "no message";
fi

