"""
Get unread feeds from tt-rss site

"""

# %% Imports


import ttrss.client
import keyring
import argparse
import textwrap

# %% Parse options

parser = argparse.ArgumentParser(description='Get tt-rss feeds.')
parser.add_argument('-U', '--url', dest='url', required=True,
                    type=str, help='TinyTiny RSS URL')
parser.add_argument('-u', '--user', dest='user', required=True,
                    type=str, help='Username')
parser.add_argument('-p', '--pass', dest='passwd', required=False,
                    type=str, help='Password')
parser.add_argument('-W', '--width', dest='text_width', default=50,
                    required=False, type=int, help='Width of text output')
parser.add_argument('-c', '--count', dest='flag_count', action='store_true',
                    required=False,
                    help='Output only the number of unread items')
parser.add_argument('-r', '--mark-read', dest='flag_mark_read',
                    action='store_true', required=False,
                    help='Mark all unread items as read')
args = parser.parse_args()

# %% Get password


if not args.passwd:
    args.passwd = keyring.get_password('tt-rss', args.user)

# %% Create connection


client = ttrss.client.TTRClient(args.url, args.user, args.passwd,
                                auto_login=True, http_auth=())
client.login()


# %% Get unread feeds


hl = client.get_headlines(view_mode='unread')


# %% Render output

if args.flag_count:
    print(len(hl))
else:
    for hh in hl:
        if args.flag_mark_read:
            client.mark_read(hh.id)
        else:
            print("\n".join([("- " if ii == 0 else "  ") + tt
                             for ii, tt in enumerate(
                                     textwrap.wrap(hh.title,
                                                   args.text_width))]))

# %% Cleanup


client.logout()
