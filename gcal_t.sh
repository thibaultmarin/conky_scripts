#!/bin/bash

# Prints a calendar and a google calendar agenda.
# Requires 'gcalcli'
#

# Get authentification
user=`cat .gauth | grep "user\s*=" | sed -r 's/user\s*=\s*(.*)$/\1/g'`;
pass=`cat .gauth | grep "pass\s*=" | sed -r 's/pass\s*=\s*(.*)$/\1/g'`;

# List of calendars
cal_list=(Home "Thibault MARIN")

# Expand list (probably a better way to do this)
cal_list_str="--cal \"${cal_list[0]}\""; 
for i in `seq 2 ${#cal_list[*]}`
do
	cal_list_str="${cal_list_str[*]} --cal \"${cal_list[$i-1]}\"";
done;

width=100;

# Get agenda
agenda=`eval gcalcli --nc $cal_list_str --user $user --pw $pass agenda`;
# Remove blank lines
agenda=`echo "$agenda"|sed '/^$/d'`;

# Split output
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

output_str="";

for s in $agenda;
do
    N=${#s};
    if [ "$N" -lt "${width}" ];
    then
        output_str="$output_str$s\n";
    else
	    output_str="$output_str${s:0:$width}\n";
        output_str="$output_str  ${s:$width:${N-$width}}\n";
    fi;
	# echo "$s";
done;

IFS=$SAVEIFS

cal -h
echo -e "$output_str";

