#!/usr/bin/python

import urllib
import unicodedata
import re
from bs4 import BeautifulSoup

# 5-day forecast page
webpage = 'http://www.weather.com/weather/5-day/Marseilles+IL+USIL0731:1:US'
# Number of days
Ndays = 2

# Get a file-like object for the Python Web site's home page.
f = urllib.urlopen(webpage)
# Read from the object, storing the page's contents in 's'.
s = f.read()
f.close()

# Parse html content
soup = BeautifulSoup(s)
p_sec = soup.find_all('p')

temps = []
temps_alt = []
phrases = []
k_temps = 0
k_temps_alt = 0
k_phrases = 0
for pi in p_sec:
	if len(pi.attrs) == 1:
		if pi.attrs['class'][0] == "wx-temp":
			temp = pi.text
			temp = re.sub("\n.*",'',temp)
			temps.append(temp)
		elif pi.attrs['class'][0] == "wx-temp-alt":
			temp_alt = pi.text
			temp_alt = re.sub("\n.*",'',temp_alt)
			temps_alt.append(temp_alt)
		elif pi.attrs['class'][0] == "wx-phrase":
			phrase = pi.text
			phrase = re.sub("\n.*",'',phrase)
			phrases.append(phrase)

temps = temps[:Ndays]
temps_alt = temps_alt[:Ndays]
phrases = phrases[:Ndays]

out_str = ''
for i in range(len(temps)):
	out_str += temps[i] + ' (' + temps_alt[i] + ') - ' + phrases[i] + '\n'

print unicodedata.normalize('NFKD', out_str[:-1]).encode('ascii','ignore')
