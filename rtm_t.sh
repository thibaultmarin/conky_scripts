#! /bin/bash
# Get task list from RTM
#
# Requires:
#  - curl
#  - A .gauth file with lines:
#      user_rtm=<username>
#      pass_rtm=<password>
#

# Get authentification
user=`cat .gauth | grep "user_rtm" | sed -r 's/user_rtm\s*=\s*(.*)$/\1/g'`;
pass=`cat .gauth | grep "pass_rtm" | sed -r 's/pass_rtm\s*=\s*(.*)$/\1/g'`;

# Get icalendar feed
tasks="`curl -s -u $user:$pass http://www.rememberthemilk.com/icalendar/$user/\
        |grep -e SUMMARY -e STATUS -e PRIORITY -e DUE|tr '\n' ' '|tr '\r' ' '\
        |sed 's/SUMMARY/\nSUMMARY/g'|grep -v \"STATUS:COMPLETED\"|grep \"DUE:\"`";

# Maximum output width
width=100;

task_str="`echo "$tasks"|sed 's/^SUMMARY:\(.*\)\s*DUE:[0-9]\{4\}\([0-9]\{2\}\)\([0-9]\{2\}\).*\s*PRIORITY:\([0-9]*\)\s*/\2\/\3-\1 #\4#/g'`";

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

output_str="";

for s in $task_str;
do
	N=${#s};
	if [ "$N" -lt "${width}" ];
	then
		output_str="$output_str$s\n";
	else
		output_str="$output_str${s:0:$width}\n";
		output_str="$output_str  ${s:$width:${N-$width}}\n";
	fi;
	# echo "$s";
done;

IFS=$SAVEIFS

# echo "output: $task_str";
echo -e "$output_str"|sed '/^$/d';

