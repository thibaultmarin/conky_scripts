#!/usr/bin/python

# Google reader access from python using api
# Extract latest unread entries from reading list.
#
# http://code.google.com/p/google-reader-api/wiki/Authentication

import requests
import re
import json
import unicodedata

# Parameters
max_width = 50
max_entries = 5
# File with authentification information
filename = '.gauth'

#Enter your username and password below within double quotes
# eg. username="username" and password="password"
COMMENT_CHAR = '#'
OPTION_CHAR =  '='

def parse_config(filename):
    options = {}
    f = open(filename)
    for line in f:
        # First, remove comments:
        if COMMENT_CHAR in line:
            # split on comment char, keep only the part before
            line, comment = line.split(COMMENT_CHAR, 1)
        # Second, find lines with an option=value:
        if OPTION_CHAR in line:
            # split on option char:
            option, value = line.split(OPTION_CHAR, 1)
            # strip spaces:
            option = option.strip()
            value = value.strip()
            # store in dictionary:
            options[option] = value
    f.close()
    return options

options = parse_config(filename)
username = options["user"]
password = options["pass"]

# Phase 1 - Get authentification token
payload = {'accountType':'GOOGLE', 'Email':username,
           'Passwd':password, 'service':'reader'}

r = requests.get("https://www.google.com/accounts/ClientLogin", params=payload)

# Parse output
auth = filter(lambda x:'Auth' in x,r.text.split('\n'))[0]
auth=re.sub('Auth=','',auth)

# Get reading list
hh = {'Authorization':'GoogleLogin auth='+auth}

r = requests.get('http://www.google.com/reader/api/0/stream/contents/user/' +
                 '-/state/com.google/reading-list', headers=hh)

# Use json library to parse output
data = json.loads(r.text)

# Browse entries (print only 'max_entries' unread)
i = 0
out_str = ''
for entry in data.items()[6][1]:
	title = entry['title']
	categories = entry['categories']
	if len(filter(lambda x:re.search('read$',x),categories)) > 0:
		break
	title = title[:50]
	out_str += title + '\n'
	i += 1
	if i == max_entries:
		break

# Cleanup message to avoid "UnicodeEncodeError: 'ascii' codec can't encode
# character" error
out_str = unicodedata.normalize('NFKD', out_str).encode('ascii','ignore')

print out_str

