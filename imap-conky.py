# -*- coding: utf-8 -*-
"""
Created on Sat Sep 20 01:41:55 2014

@author: thibault
"""

#
# IMAP access to imap inbox and folders
#
# http://yuji.wordpress.com/2011/06/22/python-imaplib-imap-example-with-gmail/


#%% Imports
import imaplib
import email
import datetime
from datetime import timezone
import time
import dateutil

from PyKDE4.kdeui import KWallet
from PyQt4 import QtGui
import textwrap

#%%

class IMAP:
	imap_url  = '127.0.0.1'
	imap_user = ''
	imap_pass = []
	wallet = []
	mail = []
	folders = ['inbox', 'status/action']
	wallet_imap_entry = 'akonadi_imap_resource_5rc'

	def __init__(self, url=None, folders=None, user=None, passwd=None):
		if url:
			self.imap_url = url
		if folders:
			self.folders = folders
		if user:
			self.imap_user = user
		if passwd:
			self.imap_pass = passwd
		else:
			self.getWallet()
			self.getPass()

	def getWallet(self):
		self.wallet = KWallet.Wallet.openWallet(
		        KWallet.Wallet.LocalWallet(), 0)
		if not self.wallet.hasFolder('imap'):
			self.wallet.createFolder('imap')
		self.wallet.setFolder('imap')

	def getPass(self):
		key, qstr_password = self.wallet.readPassword(self.wallet_imap_entry)
		# converting the password from PyQt4.QtCore.QString to str
		password_str = str(qstr_password)
		if password_str:
			self.imap_pass = password_str

	def connectIMAP(self):
		self.mail = imaplib.IMAP4_SSL(self.imap_url)
		self.mail.login(self.imap_user, self.imap_pass)
		self.mail.list()

	def getMailInFolds(self):
		emails_new = {}
		emails_cur = {}

		for fold in self.folders:

			r = self.mail.select(fold, readonly=True) # connect to folder.
			if r[0] == 'NO':
				#raise SystemError
				continue
			# Get email list in inbox
			result, data = self.mail.uid('search', None, "ALL")
			emails_uid = data[0].split()
			# Find unseen emails
			result, data_unseen = self.mail.uid('search', None, "UNSEEN")
			emails_uid_unseen = data_unseen[0].split()
			# Note: it should be possible to download all messages at once
			#result, email_data = mail.uid('fetch', ",".join(emails_uid), '(RFC822)')

			emails_new[fold] = []
			emails_cur[fold] = []

			for euid in emails_uid:

				# Fetch email
				result, email_data = self.mail.uid('fetch', euid, '(RFC822)')
				# Create email object
				raw_email = (email_data[0][1]).decode('utf-8')
				email_message = email.message_from_string(raw_email)

				# Get header information
				esub  = email_message['Subject']
				efrom = email_message['From']
				edate = email_message['Date']
				eauth = email.utils.parseaddr(efrom)[0]
				dd = dateutil.parser.parse(edate)
				ed = datetime.datetime.utcfromtimestamp(time.mktime(dd.timetuple())). \
				     replace(tzinfo=timezone.utc).astimezone(tz=None).strftime("%Y/%m/%d %H:%M")

				# Get content
				content = self.get_first_text_block(email_message).decode('utf-8','ignore')

				# Form string
				email_store = {
				    'From'   : eauth,
				    'Date'   : ed,
				    'Subject': esub,
				    'Content': content
				}

				if euid in emails_uid_unseen:
					emails_new[fold].append(email_store)
				else:
					emails_cur[fold].append(email_store)

		return emails_new, emails_cur

	def renderEmail(self, msg, max_width = 55):
		first_line = '[%s]<%s>: %s' % (msg['Date'], msg['From'], msg['Subject'])
		email_str = textwrap.wrap(first_line, max_width)[0] + "\n" + \
		            '.... %s' % textwrap.wrap(msg['Content'], max_width)[0]
		return email_str

	def renderEmails(self, emails, max_width = 55, flag_new=False):
		out_str = ''
		for fold in self.folders:
			if not fold in emails.keys():
				continue
			ff = (fold + (' (new)' if flag_new else ''))
			#ff = (fold + ' (new)') if flag_new else fold
			if emails[fold]:
				out_str += '{s:{c}^{n}}'.format(s=(' ' + ff + ' '),n=max_width+5,c='=')
				out_str += "\n"

				for e in emails[fold]:
					out_str += (self.renderEmail(e, max_width) + "\n")
		return out_str

	# note that if you want to get text content (body) and the email contains
	# multiple payloads (plaintext/ html), you must parse each message separately.
	# use something like the following: (taken from a stackoverflow post)
	def get_first_text_block(self, msg):
		maintype = msg.get_content_maintype()
		if maintype == 'multipart':
			return self.get_first_text_block(msg.get_payload()[0])
		elif maintype == 'text':
			return msg.get_payload(decode=True)

#%%

imapc = IMAP()
imapc.connectIMAP()
emails_new, emails_cur = imapc.getMailInFolds()
print(imapc.renderEmails(emails_new, flag_new = True))
print(imapc.renderEmails(emails_cur))
